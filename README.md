# Reverse string - golfed
Function that gets a string and reverses it.
Also first real code golfing.

## How the reverse function works

- Base strings

  ```js
  string = 'test'
  out = ''
  ```

- Get first character - add to beginning of out string

  ```js
  get char - 't'
  out = 't' + out
  // out is now 't'
  ```

- Get second character

  ```js
  get char - 'e'
  out = 'e' + out
  // out is now 'et'
  ```

- Get third character

  ```js
  get char - 's'
  out = 's' + out
  // out is now 'set'
  ```

- Get rest and add in same way

## The golfing process
I first started with the reverse function itself.

- Started with kinda golfed, 2 step function – [**104 byte**]
  ```js
  r=s=>{o='';for(i=s.length-1;i>=0;)o+=s[i--];return o};
  b.addEventListener('click',x=>i.value=r(i.value))
  ```

- Got first part down quite a bit, still 2 lines – [**88 byte**]
  ```js
  r=s=>{o='';for(c of s)o=c+o;return o};
  b.addEventListener('click',x=>i.value=r(i.value))
  ```

- Function in event listener to put everything in one line – [**70 byte**]
  ```js
  b.addEventListener('click',o=>{o='';for(c of i.value)o=c+o;i.value=o})
  ```

After completing that part, I wanted to also be able to change the color theme and the corresponding button text.
My base code can be seen in the ungolfed script (ts or js). It is quite long since I use objects to store the colors and themes.

- **+1 byte** (`;`) to append new code
- Variable declaration – [**+65 bytes**]
  I started with `l=true` but remembered that in JS you can also use `0` and `1` as `false` and `true` respectively. `l=!0` basically just sets `l` to the
  opposite of `0` so that `l=true` and saves **2 bytes**.
  ```js
  l=!0;a='#fff';b='#212121';e={a,b,c:'Light'};f={a:b,b:a,c:'Dark'};
  ```

- DOM manipulation function – [**+109 bytes**]
  This function gets the CSS variables set in `:root` and basically just  overwrites them with the ones we want for the theme.
  I also tried doing this with 2 functions to avoid the `r.p` but that solution was **2 bytes longer**.
  ```js
  s=c=>{r=document.documentElement.style;r.p=r.setProperty;r.p('--b',c.a);r.p ('--f',c.b);t.innerText=c.c;l=!l};
  ```

- Event listener and function call – [**+42 bytes**]
  This line adds a click event listener to a DOM object with the id `t` since no `t` is defined in JS.
  The ternary operator just checks if the theme is light and runs the corresponding function to change the theme.
  ```js
  t.addEventListener('click',z=>l?s(e):s(f))
  ```

- Put together – [**287 bytes**]
  With everything put together the code looks like this and totals a size of **287 bytes**
  ```js
  b.addEventListener('click',o=>{o='';for(c of i.value)o=c+o;i.value=o})
  l=!0;a='#fff';b='#212121';e={a,b,c:'Light'};f={a:b,b:a,c:'Dark'}
  s=c=>{r=document.documentElement.style;r.p=r.setProperty;r.p('--b',c.a);r.p ('--f',c.b);t.innerText=c.c;l=!l}
  t.addEventListener('click',z=>l?s(e):s(f))
  ```

- Update 1 – [**275 bytes**]
  I removed the object storage (`e={a,b,c:...}`) and put everything into the function call itself. Doing so saved **12 bytes** overall.
  ```js
  b.addEventListener('click',o=>{o='';for(c of i.value)o=c+o;i.value=o})
  l=!0;a='#fff';b='#212121';e='Light';f='Dark'
  s=(a,b,c)=>{r=document.documentElement.style;r.p=r.setProperty;r.p('--b',a);r.p('--f',b);t.innerText=c;l=!l}
  t.addEventListener('click',z=>l?s(a,b,e):s(b,a,f))
  ```
- Update 2 – [**269 bytes**]
  I moved the choosing of colors to the function itself. Due to that I could remove `=(a,b,c)=>` which is now just `=c=>` and calling `a` and `b` in the `s()` function.
  I did have to add ternary operators in the `r.p()` functions that are **4 bytes** longer than the previous calling of just `a` or `b`... - overall still a saving of **6 bytes**
  ```js
  s=c=>{r=document.documentElement.style;r.p=r.setProperty;r.p('--b',l?a:b);r.p('--f',l?b:a);t.innerText=c;l=!l}
  t.addEventListener('click',z=>l?s(e):s(f))
  ```

- Update 3 - [**252 bytes**]
  I moved the 'click' string for event listeners into a variable `e` and shortened `#212121` to `#222`. Also removed previous `e` and `f` (storing 'Light' and 'Dark') into `s()` call itself.
  ```js
  e='click';l=!0;f='#fff';g='#222'
  // ...
  t.addEventListener(e,z=>l?s('Light'):s('Dark'))
  ```

- Update 4 - [**251 bytes**]
  This is probably not worth it for 1 byte saved, but I got rid of calling `.addEventListener` twice. Made a new function that takes the DOM element and adds the function (second param `i`) to it.
  ```js
  // 112 byte
  b.addEventListener(e,o=>{o='';for(c of i.value)o=c+o;i.value=o})
  t.addEventListener(e,z=>l?s('Light'):s('Dark'))

  // 111 byte
  a=(h,i)=>h.addEventListener(e,i)
  a(b,o=>{o='';for(c of i.value)o=c+o;i.value=o})
  a(t,z=>l?s('Light'):s('Dark'))
  ```

- Update 5 - [**247 bytes**]
  Saved **4 bytes** by removing `e` variable that only held `'click'`
