/*
================================================================================
Reverse string function
================================================================================
*/
var input = document.querySelector('#i');
var btn = document.querySelector('#b');
var reverseStr = function (str) {
    var out = '';
    for (var _i = 0, str_1 = str; _i < str_1.length; _i++) {
        var char = str_1[_i];
        out = char + out;
    }
    return out;
};
btn.addEventListener('click', function () { return (input.value = reverseStr(input.value)); });
/*
================================================================================
Color theme declaration
================================================================================
*/
var ROOT = document.documentElement.style;
var THEME_BTN = document.querySelector('#t');
var themeIsLight = true;
var COLORS = {
    light: '#ffffff',
    dark: '#212121',
};
var THEMES = {
    light: {
        bgColor: COLORS.dark,
        fgColor: COLORS.light,
        btnText: 'Dark',
    },
    dark: {
        bgColor: COLORS.light,
        fgColor: COLORS.dark,
        btnText: 'Light',
    },
};
/*
================================================================================
Change theme function
================================================================================
*/
var setTheme = function (theme) {
    ROOT.setProperty('--b', theme.bgColor);
    ROOT.setProperty('--f', theme.fgColor);
    THEME_BTN.innerText = theme.btnText;
    themeIsLight = !themeIsLight;
};
THEME_BTN.addEventListener('click', function () {
    return themeIsLight ? setTheme(THEMES.dark) : setTheme(THEMES.light);
});
//# sourceMappingURL=script-ungolfed.js.map