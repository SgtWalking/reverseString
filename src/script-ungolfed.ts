/*
================================================================================
Reverse string function
================================================================================
*/
const input: HTMLInputElement = document.querySelector('#i');
const btn: HTMLButtonElement = document.querySelector('#b');

const reverseStr = (str: string): string => {
  let out = '';
  for (const char of str) out = char + out;
  return out;
};

btn.addEventListener('click', () => (input.value = reverseStr(input.value)));

/*
================================================================================
Color theme declaration
================================================================================
*/
const ROOT: CSSStyleDeclaration = document.documentElement.style;
const THEME_BTN: HTMLButtonElement = document.querySelector('#t');

let themeIsLight: boolean = true;

const COLORS: { light: string; dark: string } = {
  light: '#ffffff',
  dark: '#212121',
};

// Theme type
type Theme = {
  bgColor: string;
  fgColor: string;
  btnText: string;
};

const THEMES: { light: Theme; dark: Theme } = {
  light: {
    bgColor: COLORS.dark,
    fgColor: COLORS.light,
    btnText: 'Dark',
  },
  dark: {
    bgColor: COLORS.light,
    fgColor: COLORS.dark,
    btnText: 'Light',
  },
};

/*
================================================================================
Change theme function
================================================================================
*/
const setTheme = (theme: Theme) => {
  ROOT.setProperty('--b', theme.bgColor);
  ROOT.setProperty('--f', theme.fgColor);
  THEME_BTN.innerText = theme.btnText;
  themeIsLight = !themeIsLight;
};

THEME_BTN.addEventListener('click', () =>
  themeIsLight ? setTheme(THEMES.dark) : setTheme(THEMES.light)
);
